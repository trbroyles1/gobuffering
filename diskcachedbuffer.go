package buffering

import "time"

//DiskCachedBuffer behaves identically to FileCachedBuffer, but automatically uses the return value of NewFileCreateOpener as the CreateOpener to be used
//This type is provided for backward compatibility with existing dependencies on this library
type DiskCachedBuffer struct {
	FileCachedBuffer
}

//NewDiskCachedBuffer behaves identically to NewFileCachedBuffer, but automatically supplies the value of NewFileCreateOpener as the parameter value for CreateOpener
//This method is provided for backward compatibility with existing dependencies on this library
func NewDiskCachedBuffer(cachefilename string, autoSaveInterval time.Duration) (b *DiskCachedBuffer, err error) {
	fcb, err := NewFileCachedBuffer(NewFileCreateOpener(), cachefilename, autoSaveInterval)
	if err != nil {
		return nil, err
	}

	dcb := &DiskCachedBuffer{
		FileCachedBuffer: *fcb,
	}

	return dcb, nil
}
