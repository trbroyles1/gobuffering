package buffering

import (
	"bytes"
	"io"
	"sync"
)

type ThreadSafeBuffer struct {
	buf       bytes.Buffer
	bufferMtx sync.RWMutex
}

type BufferOperation func(b *ThreadSafeBuffer) error

//NewThreadSafeBuffer wraps the underlying buffer's NewBuffer() method.
func NewThreadSafeBuffer(buf []byte) *ThreadSafeBuffer {
	return &ThreadSafeBuffer{buf: *bytes.NewBuffer(buf)}
}

//Read wraps the underlying buffer's Read() method.
func (b *ThreadSafeBuffer) Read(p []byte) (int, error) {
	b.bufferMtx.Lock()
	defer b.bufferMtx.Unlock()

	return b.buf.Read(p)
}

//ReadFrom wraps the underlying buffer's ReadFrom() method.
func (b *ThreadSafeBuffer) ReadFrom(r io.Reader) (int64, error) {
	b.bufferMtx.Lock()
	defer b.bufferMtx.Unlock()

	return b.buf.ReadFrom(r)
}

//Write wraps the underlying buffer's Write() method.
func (b *ThreadSafeBuffer) Write(p []byte) (int, error) {
	b.bufferMtx.Lock()
	defer b.bufferMtx.Unlock()

	return b.buf.Write(p)
}

func (b *ThreadSafeBuffer) Prepend(p []byte) error {
	b.bufferMtx.Lock()
	defer b.bufferMtx.Unlock()

	var bufContents = b.buf.Bytes()
	var bufContentsCopy = make([]byte, len(bufContents))

	copy(bufContentsCopy, bufContents)
	bufContentsCopy = append(p, bufContentsCopy...)

	b.buf.Reset()

	var _, err = b.buf.Write(bufContentsCopy)

	return err
}

//WriteTo wraps the underlying buffer's WriteTo() method. It first checks if the DiskCachedBuffer has been closed, and returns an error of "closed" if it has been.
func (b *ThreadSafeBuffer) WriteTo(w io.Writer) (int64, error) {
	b.bufferMtx.Lock()
	defer b.bufferMtx.Unlock()

	return b.buf.WriteTo(w)
}

//Bytes wraps the underlying buffer's Bytes() method.
func (b *ThreadSafeBuffer) Bytes() []byte {
	b.bufferMtx.RLock()
	defer b.bufferMtx.RUnlock()

	return b.buf.Bytes()
}

//Do executes f in the thread-safe context of the buffer
func (b *ThreadSafeBuffer) do(f BufferOperation) error {
	b.bufferMtx.Lock()
	defer b.bufferMtx.Unlock()

	return f(b)
}
