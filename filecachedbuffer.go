package buffering

import (
	"errors"
	"io"
	"io/ioutil"
	"os"
	"sync"
	"time"
)

type CreateOpener interface {
	Create(name string) (io.ReadWriteCloser, error)
	Open(name string) (io.ReadWriteCloser, error)
}

type fileCreateOpener struct{}

func NewFileCreateOpener() *fileCreateOpener {
	return &fileCreateOpener{}
}

func (*fileCreateOpener) Create(name string) (io.ReadWriteCloser, error) {
	return os.Create(name)
}

func (*fileCreateOpener) Open(name string) (io.ReadWriteCloser, error) {
	return os.Open(name)
}

//FileCachedBuffer implements a ReadWriteCloser that attempts to persist any data left in its buffer to disk when closed. Implementation wraps a bytes.Buffer and uses
//ioutil.ReadFile to attempt to load data to the buffer on creation (via NewDiskCachedBuffer) and ioutil.WriteFile to write data to disk on close.
//Once a FileCachedBuffer has been closed, it cannot be reused.
type FileCachedBuffer struct {
	memBuffer     *ThreadSafeBuffer
	createOpener  CreateOpener
	cacheFileName string
	closed        bool
	stopSignal    chan struct{}
	stopWG        sync.WaitGroup
}

//NewFileCachedBuffer initializes a new DiskCachedBuffer, attempting to load existing data to the buffer from the file path specified in cacheFileName.
//autoSaveInterval, if greater than 0, will cause the buffer to automatically save its contents to disk on the specified interval
//If the cannot be read, the returned buffer will start empty and err will contain the underlying error. Otherwise, err will be nil.
//Note specifically that the specified cachefilename not existing is not treated as an error condition.
func NewFileCachedBuffer(co CreateOpener, cachefilename string, autoSaveInterval time.Duration) (b *FileCachedBuffer, err error) {
	b = new(FileCachedBuffer)
	b.createOpener = co
	b.cacheFileName = cachefilename
	b.stopSignal = make(chan struct{})

	var data []byte

	f, err := co.Open(cachefilename)

	if err != nil {
		if os.IsNotExist(err) {
			err = nil
			b.memBuffer = new(ThreadSafeBuffer)
		} else {
			return nil, err
		}
	} else {
		data, err = ioutil.ReadAll(f)
		if err != nil {
			f.Close()
			return nil, err
		}
		f.Close()
		b.memBuffer = NewThreadSafeBuffer(data)
	}

	b.loopSaveBufferContents(autoSaveInterval)

	return
}

//Read wraps the underlying buffers Read() method. It first checks if the DiskCachedBuffer has been closed, and returns an error of "closed" if it has been.
func (dcb *FileCachedBuffer) Read(p []byte) (int, error) {
	if dcb.closed {
		return 0, errors.New("closed")
	}
	return dcb.memBuffer.Read(p)
}

//ReadFrom wraps the underlying buffer's ReadFrom() method. It first checks if the DiskCachedBuffer has been closed, and returns an error of "closed" if it has been.
func (dcb *FileCachedBuffer) ReadFrom(r io.Reader) (int64, error) {
	if dcb.closed {
		return 0, errors.New("closed")
	}
	return dcb.memBuffer.ReadFrom(r)
}

//Write wraps the underlying buffers Write() method. It first checks if the DiskCachedBuffer has been closed, and returns an error of "closed" if it has been.
func (dcb *FileCachedBuffer) Write(p []byte) (int, error) {
	if dcb.closed {
		return 0, errors.New("closed")
	}
	return dcb.memBuffer.Write(p)
}

func (dcb *FileCachedBuffer) Prepend(p []byte) error {
	if dcb.closed {
		return errors.New("closed")
	}

	return dcb.memBuffer.Prepend(p)
}

//WriteTo wraps the underlying buffers WriteTo() method. It first checks if the DiskCachedBuffer has been closed, and returns an error of "closed" if it has been.
func (dcb *FileCachedBuffer) WriteTo(w io.Writer) (int64, error) {
	if dcb.closed {
		return 0, errors.New("closed")
	}
	return dcb.memBuffer.WriteTo(w)
}

//Close marks the DiskCachedBuffer as closed and attempts to flush any existing buffer contents to the file originally specified in the call to NewDiskCachedBuffer.
//Any returned error will be from ioutil.WriteFile
func (dcb *FileCachedBuffer) Close() error {
	dcb.closed = true
	close(dcb.stopSignal)
	dcb.stopWG.Wait()
	return dcb.saveBufferContents()
}

func (dcb *FileCachedBuffer) saveBufferContents() error {
	return dcb.memBuffer.do(func(b *ThreadSafeBuffer) error {
		f, err := dcb.createOpener.Create(dcb.cacheFileName)
		if err != nil {
			return err
		}
		defer f.Close()
		_, err = f.Write(b.buf.Bytes())
		return err
	})
}

func (dcb *FileCachedBuffer) loopSaveBufferContents(autoSaveInterval time.Duration) {
	if autoSaveInterval > 0 {
		t := time.NewTicker(8 * time.Second)
		dcb.stopWG.Add(1)
		go func() {
			defer dcb.stopWG.Done()
			for {
				select {
				case <-dcb.stopSignal:
					return
				case <-t.C:
					dcb.saveBufferContents()
				}
			}
		}()
	}
}
